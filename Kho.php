<?php
include_once 'May.php';
class Kho{
    private $danhSachMay = [], $soLuongMay, $tenKho;
    public function __construct()
    {
        $this->soLuongMay = 0;
    }

    public function nhap() {
        $ds = new Kho();
        $ds->danhSachMay = $this->danhSachMay;
        echo '+++++++++++++ Nhập kho +++++++++++++' . "\n";

        do{
            $this->tenKho = readline('Nhập tên kho: ');

            if($this->tenKho == ''){
                echo 'Bạn chưa nhập tên kho. Xin kiểm tra lại.' . "\n";
            }
        } while($this->tenKho == '');


        $soLuongMay = null;
        do{
            $soLuongMay = readline('Nhập số lượng máy: ');
            if ($soLuongMay == ''){
                echo 'Bạn chưa nhập số lượng máy. Xin kiểm tra lại' . "\n";
            }

            else if (!is_numeric($soLuongMay)){
                echo 'Số lượng máy không hợp lệ. Xin kiểm tra
                 lại.' . "\n";
            }
            else if ($soLuongMay < 1){
                echo 'Số lượng chi tiết phải lớn hơn 0. Xin kiểm tra lại.' . "\n";
            }
        } while(!is_numeric($soLuongMay) || $soLuongMay == '' || $soLuongMay < 1);

        for ($i = 0; $i < $soLuongMay; ++$i){
            echo " -> Nhập máy thứ " . ($i + 1) . ": \n";
            $temp = new May();
            $temp->nhap();
            array_push($ds->danhSachMay, $temp);
        }

        $this->soLuongMay += (int)$soLuongMay; //
        $this->danhSachMay = $ds->danhSachMay; //

        $this->xuat();
        $this->luaChonChucNang();
    }

    public function xuat(){
        echo "\t\t" . '+++++++++++++++++++++++++++++++ DANH SÁCH KHO +++++++++++++++++++++++++++++++' . "\n";

        for ($i = 0; $i < $this->soLuongMay; ++$i){
            echo "\t -> -> -> -> -> Máy Thứ " . ($i + 1) . "<- <- <- <- <- \n";
            $this->danhSachMay[$i]->xuat();
        }

        $this->luaChonChucNang();
    }

    public function luaChonChucNang () {
        echo '$$$$$$$$$$$$$$$$$$$$$$$ QUẢN LÝ KHO $$$$$$$$$$$$$$$$$$$$$$$' . "\n\n";
        echo '* * * * * * * * * *  MENU CHỨC NĂNG * * * * * * * * * * * *' . "\n";
        echo '*                   1. Nhập kho                           *' . "\n";
        echo '*                   2. Xuất kho                           *' . "\n";
        echo '*                   3. Tìm kiếm máy                       *' . "\n";
        echo '*                   4. Thống kê                           *' . "\n";
        echo '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *' . "\n";

        $luaChon = null;
        do{
            $luaChon = readline('Nhập lựa chọn của bạn: ');

            switch ($luaChon){
                case 1: $this->nhap(); break;
                case 2: $this->xuat(); break;
                case 3: $this->timMay($this->danhSachMay); break;
                case 4: $this->thongKeMay(); break;
            }
        } while($luaChon < 1 || $luaChon > 4);


    }

    public function timMay($dsMay){
        $str_search = readline('Nhập tên máy, giá máy, khối lượng máy, mã chi tiết....: ');
        for ($i = 0; $i < count($dsMay); ++$i)
        {
            echo $dsMay[$i]->getDanhSachChiTiet()->getMaSo();
        }

        $check = false;
        if ($str_search != '')
        {
            if (count($dsMay) > 0)
            {
                for ($i = 0; $i < count($dsMay); ++$i)
                {
                    if (strstr($dsMay[$i]->getTenMay(), $str_search)
                        || $dsMay[$i]->tinhKhoiLuong() == $str_search
                        || $dsMay[$i]->tinhGia() == $str_search
                        || $dsMay[$i]->getDanhSachChiTiet()->getMaSo() == $str_search) {
                        echo '▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼ Kết quả tìm được: ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼' . "\n";
                        $this->danhSachMay[$i]->xuat();
                        $check = true;
                        break;
                    }
                }
            }

            if (!$check)
            {
                echo '!!!!!! Không tìm thấy máy nào trong kho !!!!!!' . "\n";
            }
        }


        $this->luaChonChucNang();
    }

    public function thongKeMay(){
        echo '======================== THỐNG KÊ ========================' . "\n";
        echo "\t" . '=> Tổng số máy: ' . $this->soLuongMay;
        echo "\t" . '=> Tổng khối lượng: ' . $this->tinhTongKhoiLuong();
        echo "\t" . '=> Tổng tiền: ' . $this->tinhTongGia();

        $this->luaChonChucNang();
    }

    public function tinhTongKhoiLuong(){
        $tongKhoiLuong = 0;
        for ($i = 0; $i < $this->soLuongMay; ++$i)
        {
            $tongKhoiLuong += $this->danhSachMay[$i]->tinhKhoiLuong();
        }
        return $tongKhoiLuong;
    }

    public function tinhTongGia(){
        $tongGia = 0;
        for ($i = 0; $i < $this->soLuongMay; ++$i)
        {
            $tongGia += $this->danhSachMay[$i]->tinhGia();
        }
        return $tongGia;
    }

    public function test() {
        $str = " hello";
        if(strstr($str, 'l1'))
        {
            echo 'found';
        }
        else
            echo 'not found';

    }
}

$a = new Kho();
$a->luaChonChucNang();
//$a->test();



