<?php
include_once "ChiTiet.php";
include_once  "ChiTietDon.php";

class ChiTietPhuc extends ChiTiet{

    private $soLuongChiTiet;
    private $danhSachChiTiet = [];

    public function nhap()
    {
        do {
            $this->soLuongChiTiet = readline('Nhập số lượng chi tiết con: ');

            if($this->soLuongChiTiet == ''){
                echo 'Bạn chưa nhập số lượng chi tiết con. Xin kiểm tra lại' . "\n";
            }
            else if(!is_numeric($this->soLuongChiTiet)){
                echo 'Nhập số lượng không hợp lệ. Xin kiểm tra lại' . "\n";
            }
            else if($this->soLuongChiTiet <= 0){
                echo 'Số lượng chi tiết con phải lớn hơn 0. Xin kiểm tra lại' . "\n";
            }
        } while(!is_numeric($this->soLuongChiTiet) || $this->soLuongChiTiet == '' || $this->soLuongChiTiet <= 0);

        for ($i = 0; $i < $this->soLuongChiTiet; ++$i){

            echo '----------- Nhập thông tin của chi tiết thứ ' . ($i+1) . "-----------\n";

            parent::nhap();
            $loaiChiTiet = null;
            echo "─────────────────────────────\n";
            echo "|     CHỌN LOẠI CHI TIẾT    |\n";
            echo "|    1. Loại Chi tiết đơn   |\n";
            echo "|    2. Loại Chi tiết phức  |\n";
            echo "─────────────────────────────\n";

            do{
                $loaiChiTiet = readline('Lựa chọn của bạn: ');

                if($loaiChiTiet == ''){
                    echo 'Bạn chưa nhập lựa chọn. Xin kiểm tra lại' . "\n";
                }

                else if($loaiChiTiet < 1 || $loaiChiTiet > 2){
                    echo 'Lựa chọn không hợp lệ. Xin kiểm tra lại' . "\n";
                }
            } while($loaiChiTiet == '' || ($loaiChiTiet < 1 || $loaiChiTiet > 2));

            if ($loaiChiTiet == 1){
                $chiTiet = new ChiTietDon();

            }
            else {
                $chiTiet = new ChiTietPhuc();
            }

            $chiTiet->nhap();
            $this->danhSachChiTiet[] = $chiTiet;
        }
    }

    public function xuat()
    {
        parent::xuat();

        echo '>>>>>>>>>>>>>>> Danh sách chi tiết con (gồm ' . $this->soLuongChiTiet . ' chi tiết ) <<<<<<<<<<<<<<<' . "\n";
        echo "\t" . '=> Tổng khối lượng: ' . $this->tinhKhoiLuong() . "\n";
        echo "\t" . '=> Tổng tiền: ' . $this->tinhTien() . "\n";

        for($i = 0; $i < $this->soLuongChiTiet; ++$i){
            $this->danhSachChiTiet[$i]->xuat();
        }
    }

    public function tinhKhoiLuong()
    {
        $tongKhoiLuong = 0;
        for($i = 0; $i < $this->soLuongChiTiet; ++$i){
            $tongKhoiLuong += $this->danhSachChiTiet[$i]->tinhKhoiLuong();
        }
        return $tongKhoiLuong;
    }

    public function tinhTien()
    {
        $tongTien = 0;
        for($i = 0; $i < $this->soLuongChiTiet; ++$i){
            $tongTien += $this->danhSachChiTiet[$i]->tinhTien();
        }
        return $tongTien;
    }

    public function timChiTiet($str_search){

    }
}
//
//$a = new ChiTietPhuc();
//$a->nhap();
//$a->xuat();