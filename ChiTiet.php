<?php
abstract class ChiTiet {
    private $maSo;

    public function nhap(){
        $this->maSo = readline("Nhập mã số : ");
    }

    public function xuat(){
        echo '================ Thông Tin Chi Tiết ================' . "\n";
        echo "\t ->  Mã số của chi tiet là: $this->maSo \n";
    }

    public function getMaSo()
    {
        return $this->maSo;
    }

    public abstract function tinhKhoiLuong();
    public abstract function tinhTien();
}
