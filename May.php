<?php
include_once 'ChiTietPhuc.php';
class May {
    private $maSo, $tenMay, $danhSachChiTiet;

    public function nhap(){

        do{
            $this->maSo = readline('Nhập mã số máy: ');

            if($this->maSo == ''){
                echo 'Bạn chưa nhập mã số máy. Xin kiểm tra lại.' . "\n";
            }
        } while($this->maSo == '');

        do{
            $this->tenMay = readline('Nhập tên máy: ');

            if($this->tenMay == ''){
                echo 'Bạn chưa nhập tên máy. Xin kiểm tra lại' . "\n";
            }
        } while($this->tenMay == '');

        echo '= =====>> Nhập danh sách chi tiết của máy: ' . "\n";

        $this->danhSachChiTiet = new ChiTietPhuc();
        $this->danhSachChiTiet->nhap();
    }

    public function xuat(){
        echo ' * * * * * * * * Thông tin máy * * * * * * * * ' . "\n";
        echo ' => Mã số máy: '. $this->maSo . "\n";
        echo ' => Tên máy: ' . $this->tenMay . "\n";
        $this->danhSachChiTiet->xuat();
        echo '=================================================================' . "\n";
        echo 'Giá thành = ' . $this->tinhGia() . "\n";
        echo 'Tổng khối lượng = ' . $this->tinhKhoiLuong() . "\n";
    }

    public function tinhKhoiLuong(){
        return $this->danhSachChiTiet->tinhKhoiLuong();
    }

    public function tinhGia(){
        return $this->danhSachChiTiet->tinhTien();
    }

    public function getTenMay()
    {
        return $this->tenMay;
    }

    public function getDanhSachChiTiet()
    {
        return $this->danhSachChiTiet;
    }
}

//$a = new May();
//$a->nhap();
//$a->xuat();

