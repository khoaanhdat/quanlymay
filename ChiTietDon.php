<?php
include_once "ChiTiet.php";

class ChiTietDon extends ChiTiet{

    private $gia, $khoiLuong;

    public function nhap()
    {
        parent::nhap();
        do {
            $this->gia = readline('Nhập giá: ');

            if (!is_numeric($this->gia)){
                echo 'Giá nhập không hợp lệ. Xin kiểm tra lại.' . "\n";
            }

            else if ($this->gia == ''){
                echo 'Bạn chưa nhập giá. Xin kiểm tra lại' . "\n";
            }

            else if ($this->gia < 0 ){
                echo 'Giá chi tiết phải lớn hơn 0. Xin kiểm tra lại' . "\n";
            }
        } while(!is_numeric($this->gia) || $this->gia == '' || $this->gia < 0);

        do{
            $this->khoiLuong = readline('Nhập khối lượng(kg): ');

            if (!is_numeric($this->khoiLuong)){
                echo 'Khối lượng nhập không hợp lệ. Xin kiểm tra lại.' . "\n";
            }
            else if ($this->khoiLuong == ''){
                echo 'Bạn chưa nhập khối lượng. Xin kiểm tra lại' . "\n";
            }
            else if ($this->khoiLuong <= 0){
                echo 'Khối lượng phải lớn hơn 0. Xin kiểm tra lại' . "\n";
            }
        } while(!is_numeric($this->khoiLuong) || $this->gia == '' || $this->khoiLuong <= 0);

    }

    public function xuat()
    {
        parent::xuat();
        echo "\t ->  Giá của chi tiết đơn này: $this->gia \n";
        echo "\t ->  Khối lượng của chi tiết này: $this->khoiLuong \n";
    }

    public function tinhKhoiLuong()
    {
        return $this->khoiLuong;
    }

    public function tinhTien()
    {
        return $this->gia;
    }
}

$a = new ChiTietDon();
//$a->nhap();
//$a->xuat();